#include <jni.h>

#include "graph.h"

extern "C" JNIEXPORT jbyte JNICALL
__unused Java_pub_nya_saber_mctf_MainActivity_getCharFromMatrix(
		JNIEnv /* *env */,
		jobject /* this */,
		jint i,
		jint j) {
#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve" // graph.h is too big for Android Studio
	if (i >= matrix_size || j >= matrix_size) return -1;
	return matrix[i][j];
#pragma clang diagnostic pop
}
