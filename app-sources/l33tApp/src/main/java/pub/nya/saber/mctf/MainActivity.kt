package pub.nya.saber.mctf

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Vibrator
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val tag: String = this::class.java.simpleName
    private var i = -1
    private var j = 0
    private var isEnd = false
    private var vibrator: Vibrator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.v(tag, "onCreate()")
        setContentView(R.layout.activity_main)
        setSupportActionBar(Toolbar_root)
        vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?

        getNewNode()
        Button_next.setOnClickListener {
            @Suppress("DEPRECATION")
            vibrator?.vibrate(25)
            getNewNode()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.MenuItem_about -> {
            startActivity(Intent(this, AboutActivity::class.java))
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun getNewNode() {
        ++i
        if (i >= matrixSize) {
            i = 0
            ++j
        }
        if (i >= 500) { // mehehehehehehe!
            @Suppress("DEPRECATION")
            vibrator?.vibrate(200)
            Log.e(tag, "Nanomachines, son!")
            EditText_currentNode.setText(R.string.MainActivity_editText_currentNode_meh)
            if (isEnd) {
                @Suppress("DEPRECATION")
                vibrator?.vibrate(500)
                Toast.makeText(this, "What did i just say?", Toast.LENGTH_LONG).show()
            }
            isEnd = true
            return
        }
        val newChar: Byte = getCharFromMatrix(i, j)
        if (newChar.compareTo(-1) == 0) {
            Log.e(tag, "wtf, newChar is null!")
            return
        }
        EditText_currentNode.setText(newChar.toString())
        Log.v(tag, "got ${newChar.toChar()}")
        TextView_i.text = String.format(getString(R.string.MainActivity_layout_textView_i), i)
        TextView_j.text = String.format(getString(R.string.MainActivity_layout_textView_j), j)
    }

    private external fun getCharFromMatrix(i: Int, j: Int): Byte

    companion object {
        const val matrixSize = 1000

        init {
            System.loadLibrary("ctf-lib")
        }
    }
}
