"""Конвертирует матрицу смежности из CSV в C++-инициализацию матрицы."""

matrix_content = ''
lines_count = 0
with open(r'C:\Users\saber-nyan\Desktop\v2_matrix.csv', 'rt', encoding='utf8') as f:
    next(f)  # skip first line
    for line in f:
        lines_count += 1
        matrix_content += '\t{'
        splitted = line.split(',')[1:]
        for element in splitted:
            matrix_content += element.replace('\n', '') + ', '
        matrix_content += '},\n'

out = f'''
static const int matrix_size = {lines_count};
static char matrix[matrix_size][matrix_size] = {{
{matrix_content}
}};'''

with open(r'C:\Users\saber-nyan\Desktop\v2_matrix.cpp', 'wt', encoding='utf8') as f:
    f.write(out)
